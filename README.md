# CinemaBookingSystem
Website for booking cinema Ticket
CinemaBookingSystem
Website for booking cinema Ticket

# To run the app, open the cmd, change the directory to where the manage.py is in (CinemaBookingSystem/manage.py) 

# create a virtual environment, 'myenv' is the name of the environment

py -m venv myenv

# run the virtual environment

myenv\scripts\activate

# Install Django

pip install django

# Crispy_form

pip install django-crispy-forms

# Bootstrap 5

pip install django-bootstrap-v5

# Crispy Boostrap 5

pip install crispy-bootstrap5

# xhtml2pdf

pip install xhtml2pdf

# pillow 

pip install Pillow

# This is a test
